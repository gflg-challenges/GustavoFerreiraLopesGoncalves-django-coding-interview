from django.urls import path
from .views import StudentListCreateView, StudentUpdateView


urlpatterns = [
    path('students/', StudentListCreateView.as_view(), name='get-students-list'),
    path('students/<int:pk>', StudentUpdateView.as_view(), name='update-student'),
]
