from django.db import models
from subjects.models import Subject

# Create your models here.
class Student(models.Model):
    """Students model."""

    name = models.CharField(max_length=128)
    completed = models.BooleanField()
    subjects = models.ManyToManyField(Subject)

    def __str__(self):
        return self.name
